import React, { Component } from 'react';
import './App.css';


class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      "todos": [
        {"label":"rbac", "completed": false},
        {"label":"react", "completed": false},
        {"label":"test", "completed": false},
        {"label":"build", "completed": false},
      ],
      "todo": ""
    }
    this.todoChanged = this.todoChanged.bind(this)
    this.addTodo = this.addTodo.bind(this)
    this.toggle = this.toggle.bind(this)
  }
  todoChanged(event){
    this.setState({
      "todo": event.target.value
    })
  }
  addTodo(event){
    const todos = this.state.todos
    const todo = this.state.todo
    this.setState({
      todos: [
        ...todos,
        {label: todo, completed: false}
      ],
      todo: ""
    })
  }
  toggle(index, event){
    const todos = this.state.todos
    const todo = todos[index]
    this.setState({
      todos: [
        ...todos.slice(0, index),
        Object.assign({}, todo, {completed: !todo.completed}),
        ...todos.slice(index+1)
      ]
    })
  }
  render() {
    return (
      <div>
        <ul>
        {this.state.todos.map((todo, index)=><li onClick={(evt) => this.toggle(index, evt)} key={index} style={todo.completed ? {"text-decoration": "line-through"} : {}}>{todo.label}</li>)}
        </ul>
        <input type="text" onChange={this.todoChanged} value={this.state.todo} />
        <button onClick={this.addTodo}>Add</button>
      </div>
    );
  }
}

export default App;
